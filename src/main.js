// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Navigation from '@/components/Nav.vue'
import router from './router'
import VueYouTubeEmbed from 'vue-youtube-embed'
import VueScrollTo from 'vue-scrollto'

Vue.use(VueScrollTo,{
     container: "body",
     duration: 500,
     easing: "ease",
     offset: 0,
     cancelable: true,
     onDone: false,
     onCancel: false,
     x: false,
     y: true
 })
Vue.use(VueYouTubeEmbed)
Vue.config.productionTip = false;
window.eventHub = new Vue(); // Single event hub


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App, Navigation }
})
